#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>

#define LIBNET_LIL_ENDIAN 1

void usage()
{
  printf("syntax: pcap-test <interface>\n");
  printf("sample: pcap-test wlan0\n");
}

typedef struct
{
  char *dev_;
} Param;

Param param = {
    .dev_ = NULL};

struct libnet_ethernet_hdr
{
  u_int8_t ether_dhost[6]; /* destination ethernet address */
  u_int8_t ether_shost[6]; /* source ethernet address */
  u_int16_t ether_type;    /* protocol */
};

struct libnet_ipv4_hdr
{
#if (LIBNET_LIL_ENDIAN)
  u_int8_t ip_hl : 4, /* header length */
      ip_v : 4;       /* version */
#endif
#if (LIBNET_BIG_ENDIAN)
  u_int8_t ip_v : 4, /* version */
      ip_hl : 4;     /* header length */
#endif
  u_int8_t ip_tos; /* type of service */
#ifndef IPTOS_LOWDELAY
#define IPTOS_LOWDELAY 0x10
#endif
#ifndef IPTOS_THROUGHPUT
#define IPTOS_THROUGHPUT 0x08
#endif
#ifndef IPTOS_RELIABILITY
#define IPTOS_RELIABILITY 0x04
#endif
#ifndef IPTOS_LOWCOST
#define IPTOS_LOWCOST 0x02
#endif
  u_int16_t ip_len; /* total length */
  u_int16_t ip_id;  /* identification */
  u_int16_t ip_off;
#ifndef IP_RF
#define IP_RF 0x8000 /* reserved fragment flag */
#endif
#ifndef IP_DF
#define IP_DF 0x4000 /* dont fragment flag */
#endif
#ifndef IP_MF
#define IP_MF 0x2000 /* more fragments flag */
#endif
#ifndef IP_OFFMASK
#define IP_OFFMASK 0x1fff /* mask for fragmenting bits */
#endif
  u_int8_t ip_ttl;               /* time to live */
  u_int8_t ip_p;                 /* protocol */
  u_int16_t ip_sum;              /* checksum */
  struct in_addr ip_src, ip_dst; /* source and dest address */
};

struct libnet_tcp_hdr
{
  u_int16_t th_sport; /* source port */
  u_int16_t th_dport; /* destination port */
  u_int32_t th_seq;   /* sequence number */
  u_int32_t th_ack;   /* acknowledgement number */
#if (LIBNET_LIL_ENDIAN)
  u_int8_t th_x2 : 4, /* (unused) */
      th_off : 4;     /* data offset */
#endif
#if (LIBNET_BIG_ENDIAN)
  u_int8_t th_off : 4, /* data offset */
      th_x2 : 4;       /* (unused) */
#endif
  u_int8_t th_flags; /* control flags */
#ifndef TH_FIN
#define TH_FIN 0x01 /* finished send data */
#endif
#ifndef TH_SYN
#define TH_SYN 0x02 /* synchronize sequence numbers */
#endif
#ifndef TH_RST
#define TH_RST 0x04 /* reset the connection */
#endif
#ifndef TH_PUSH
#define TH_PUSH 0x08 /* push data to the app layer */
#endif
#ifndef TH_ACK
#define TH_ACK 0x10 /* acknowledge */
#endif
#ifndef TH_URG
#define TH_URG 0x20 /* urgent! */
#endif
#ifndef TH_ECE
#define TH_ECE 0x40
#endif
#ifndef TH_CWR
#define TH_CWR 0x80
#endif
  u_int16_t th_win; /* window */
  u_int16_t th_sum; /* checksum */
  u_int16_t th_urp; /* urgent pointer */
};

bool parse(Param *param, int argc, char *argv[])
{
  if (argc != 2)
  {
    usage();
    return false;
  }
  param->dev_ = argv[1];
  return true;
}

void DumpHex(const void *data, int size)
{ // for dump packet
  char ascii[17];
  int i, j;
  ascii[16] = '\0';
  for (i = 0; i < size; ++i)
  {
    printf("%02X ", ((unsigned char *)data)[i]);
    if (((unsigned char *)data)[i] >= ' ' && ((unsigned char *)data)[i] <= '~')
    {
      ascii[i % 16] = ((unsigned char *)data)[i];
    }
    else
    {
      ascii[i % 16] = '.';
    }
    if ((i + 1) % 8 == 0 || i + 1 == size)
    {
      printf(" ");
      if ((i + 1) % 16 == 0)
      {
        printf("|  %s \n", ascii);
      }
      else if (i + 1 == size)
      {
        ascii[(i + 1) % 16] = '\0';
        if ((i + 1) % 16 <= 8)
        {
          printf(" ");
        }
        for (j = (i + 1) % 16; j < 16; ++j)
        {
          printf("   ");
        }
        printf("|  %s \n", ascii);
      }
    }
  }
}

int main(int argc, char *argv[])
{
  if (!parse(&param, argc, argv))
    return -1;

  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t *pcap = pcap_open_live(param.dev_, BUFSIZ, 1, 1000, errbuf);
  if (pcap == NULL)
  {
    fprintf(stderr, "pcap_open_live(%s) return null - %s\n", param.dev_, errbuf);
    return -1;
  }

  while (true)
  {
    struct pcap_pkthdr *header;
    const u_char *packet;
    int res = pcap_next_ex(pcap, &header, &packet);
    if (res == 0)
      continue;
    if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
    {
      printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
      break;
    }
    printf("%u bytes captured\n", header->caplen);

    // Parse Ethernet
    struct libnet_ethernet_hdr *ethernet_hdr = malloc(sizeof(struct libnet_ethernet_hdr));
    memcpy(ethernet_hdr, packet, 14);

    if (ethernet_hdr->ether_type == 0x8)
    { // if IPv4
      // Parse IP
      struct libnet_ipv4_hdr *ipv4_hdr = malloc(sizeof(struct libnet_ipv4_hdr));
      memcpy(ipv4_hdr, packet + sizeof(struct libnet_ethernet_hdr), sizeof(struct libnet_ipv4_hdr));
      if (ipv4_hdr->ip_p == 6)
      { // if TCP
        // Parse TCP
        struct libnet_tcp_hdr *tcp_hdr = malloc(sizeof(struct libnet_tcp_hdr));
        memcpy(tcp_hdr, packet + sizeof(struct libnet_ethernet_hdr) + (ipv4_hdr->ip_hl * 4), sizeof(struct libnet_tcp_hdr));

        // Parse Payload
        u_int8_t *payload = malloc(sizeof(payload));
        memset(payload, 0, sizeof(payload));
        memcpy(payload, packet + sizeof(struct libnet_ethernet_hdr) + (ipv4_hdr->ip_hl * 4) + (tcp_hdr->th_off * 4), 10);

        printf("==================\n");
        printf("[Ethernet header]\n");
        printf("Source MAC Address: %02x:%02x:%02x:%02x:%02x:%02x\n", ethernet_hdr->ether_shost[0], ethernet_hdr->ether_shost[1], ethernet_hdr->ether_shost[2], ethernet_hdr->ether_shost[3], ethernet_hdr->ether_shost[4], ethernet_hdr->ether_shost[5]);
        printf("Destination MAC Address: %02x:%02x:%02x:%02x:%02x:%02x\n", ethernet_hdr->ether_dhost[0], ethernet_hdr->ether_dhost[1], ethernet_hdr->ether_dhost[2], ethernet_hdr->ether_dhost[3], ethernet_hdr->ether_dhost[4], ethernet_hdr->ether_dhost[5]);
        printf("[IP header]\n");
        printf("Source IP: %s\n", inet_ntoa(ipv4_hdr->ip_src));
        printf("Destination IP: %s\n", inet_ntoa(ipv4_hdr->ip_dst));
        printf("[TCP header]\n");
        printf("Source Port: %d\n", ntohs(tcp_hdr->th_sport));
        printf("Destination Port: %d\n", ntohs(tcp_hdr->th_dport));
        printf("[Payload]\n");
        printf("Payload(10 byte): %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", payload[0], payload[1], payload[2], payload[3], payload[4], payload[5], payload[6], payload[7], payload[8], payload[9]);
        printf("==================\n");

        free(payload);
        free(tcp_hdr);
      }
      free(ipv4_hdr);
    }
    free(ethernet_hdr);
  }

  pcap_close(pcap);
}